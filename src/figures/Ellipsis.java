/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figures;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.util.Random;

/**
 *
 * @author engray
 */
public class Ellipsis extends Figure {
    Random generator = new Random();
    int rand1 = generator.nextInt(50);
    int rand2 = generator.nextInt(100);
    int rand3 = generator.nextInt(30);
    int rand4 = generator.nextInt(50);
    public Ellipsis(Graphics2D buf, int del, int w, int h) {
        
 

        super(buf, del, w, h);
        shape = new Ellipse2D.Double(rand1, rand2, rand3, rand4);
        aft = new AffineTransform();                                  
        area = new Area(shape);
    }

    
    
}
